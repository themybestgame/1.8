using UnityEngine;

public class SubVector : MonoBehaviour
{

    [SerializeField] Transform Obj1;
    [SerializeField] Transform Obj2;

    void Update()
    {
        Sub();
        Log();
    }

    void Sub()
    {
        transform.position = Obj1.position - Obj2.position;
    }

    void Log()
    {
        Debug.Log("Координаты 1 объекта " + Obj1.position);
        Debug.Log("Координаты 2 объекта " + Obj2.position);
        Debug.Log("Результат вычитания 2х векторов "+transform.position);
    }
}
