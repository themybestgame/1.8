using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField] Vector3 angVelosity;

    private void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        transform.Rotate(angVelosity * Time.deltaTime);
    }
}