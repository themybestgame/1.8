using UnityEngine;

public class Distance : MonoBehaviour
{

    [SerializeField] Transform Obj1;
    [SerializeField] Transform Obj2;

    void Update()
    {
        Sub();
        Log();
    }

    void Sub()
    {
        transform.position = new Vector3(0, Vector3.Distance(Obj1.position, Obj2.position), 0);
    }

    void Log()
    {
        Debug.Log("Координаты 1 объекта " + Obj1.position);
        Debug.Log("Координаты 2 объекта " + Obj2.position);
        Debug.Log("Расстояние между двумя обьектами "+transform.position.y);
    }
}
