using UnityEngine;

public class MultVector : MonoBehaviour
{

    [SerializeField] Transform Obj1;
    [SerializeField] float mult;

    void Update()
    {
        Mult();
        Log();
    }

    void Mult()
    {
        transform.position = Obj1.position*mult;
    }

    void Log()
    {
        Debug.Log("Координаты объекта " + Obj1.position);
        Debug.Log("Множитель вектора " + mult);
        Debug.Log("Результат умножения вектора "+Obj1.position+" на число "+mult+" равен "+transform.position);
    }
}
